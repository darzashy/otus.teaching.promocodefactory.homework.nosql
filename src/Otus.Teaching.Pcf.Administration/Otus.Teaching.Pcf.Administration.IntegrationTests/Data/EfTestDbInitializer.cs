﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class EfTestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public EfTestDbInitializer(IMongoDatabase dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            var employees = _dataContext.GetCollection<Employee>(nameof(Employee));
            var roles = _dataContext.GetCollection<Role>(nameof(Role));
            
            if (roles.Find(_ => true).Any<Role>() ||
                employees.Find(_ => true).Any<Employee>())
                return;

            roles.InsertManyAsync(TestDataFactory.Roles);
            employees.InsertManyAsync(TestDataFactory.Employees);
        }

        public void CleanDb()
        {
            var employeeCollection = _dataContext.GetCollection<Employee>(nameof(Employee));
            var roleCollection = _dataContext.GetCollection<Role>(nameof(Role));

            if (roleCollection.EstimatedDocumentCount() > 0)
                roleCollection.DeleteMany(_ => true);

            if (employeeCollection.EstimatedDocumentCount() > 0)
                employeeCollection.DeleteMany(_ => true);

        }
    }
}