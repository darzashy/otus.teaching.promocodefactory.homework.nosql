﻿using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public EfDbInitializer(IMongoDatabase dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            var employees = _dataContext.GetCollection<Employee>("Employee");
            var roles = _dataContext.GetCollection<Role>("Role");
            
            if (roles.Find(_ => true).Any<Role>() ||
                employees.Find(_ => true).Any<Employee>())
                return;

            roles.InsertManyAsync(FakeDataFactory.Roles);
            employees.InsertManyAsync(FakeDataFactory.Employees);
        }
    }
}