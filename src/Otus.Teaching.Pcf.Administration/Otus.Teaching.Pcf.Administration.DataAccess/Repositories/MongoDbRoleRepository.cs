﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRoleRepository : IRepository<Role>
    {
        private readonly IMongoCollection<Role> _roles;

        public MongoDbRoleRepository(IMongoDatabase dataContext)
        {
            _roles = dataContext.GetCollection<Role>(nameof(Role));
        }

        public async Task<IEnumerable<Role>> GetAllAsync()
        {
            return await _roles
                .Find(_ => true)
                .SortBy(x => x.Name)
                .ToListAsync();
        }

        public async Task<Role> GetByIdAsync(Guid id)
        {
            var role = await _roles
                .Find(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (role == null)
            {
                throw new Exception($"Unable to find Role {id}");
            }

            return role;
        }

        public async Task<IEnumerable<Role>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _roles.Find(x => ids.Contains(x.Id))
                .SortBy(x => x.Name)
                .ToListAsync();
        }

        public async Task<Role> GetFirstWhere(Expression<Func<Role, bool>> predicate)
        {
            return await _roles.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Role>> GetWhere(Expression<Func<Role, bool>> predicate)
        {
            return await _roles
                .Find(predicate)
                .SortBy(x => x.Name)
                .ToListAsync();
        }

        public async Task AddAsync(Role entity)
        {
            await _roles.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Role entity)
        {
            var filter = Builders<Role>.Filter.Eq(nameof(entity.Id), entity.Id);
            var result = await _roles.ReplaceOneAsync(filter, entity);
            if (result.IsAcknowledged == false)
                throw new Exception($"Unable to update Role {entity.Id}");
        }

        public async Task DeleteAsync(Role entity)
        {
            var result = await _roles.DeleteOneAsync(x => x.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception($"Unable to delete Role {entity.Id}");
        }
    }
}