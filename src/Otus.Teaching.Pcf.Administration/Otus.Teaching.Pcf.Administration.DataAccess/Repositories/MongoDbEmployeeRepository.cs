﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbEmployeeRepository : IRepository<Employee>
    {
        private readonly IMongoCollection<Employee> _employees;

        public MongoDbEmployeeRepository(IMongoDatabase dataContext)
        {
            _employees = dataContext.GetCollection<Employee>(nameof(Employee));
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _employees.Find(_ => true)
                .SortBy(x => x.LastName)
                .ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            var entity = await _employees.Find(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                throw new Exception($"Unable to find Employee {id}");
            }

            return entity;
        }

        public async Task<IEnumerable<Employee>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _employees.Find(x => ids.Contains(x.Id))
                .SortBy(x => x.LastName)
                .ToListAsync();
        }

        public async Task<Employee> GetFirstWhere(Expression<Func<Employee, bool>> predicate)
        {
            return await _employees.Find(predicate)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Employee>> GetWhere(Expression<Func<Employee, bool>> predicate)
        {
            return await _employees.Find(predicate)
                .SortBy(x => x.LastName)
                .ToListAsync();
        }

        public async Task AddAsync(Employee entity)
        {
            await _employees.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(Employee entity)
        {
            var filter = Builders<Employee>.Filter.Eq(nameof(entity.Id), entity.Id);
            var result = await _employees.ReplaceOneAsync(filter, entity);
            if (result.IsAcknowledged == false)
                throw new Exception($"Unable to update Employee {entity.Id}");
        }

        public async Task DeleteAsync(Employee entity)
        {
            var result = await _employees.DeleteOneAsync( x => x.Id == entity.Id);
            if (result.IsAcknowledged == false)
                throw new Exception($"Unable to delete Employee {entity.Id}");
        }

    }
}